# Getting Started
Just a simple rest-based microservice with the following API
* /persons
* /persons?vorname=
* /persons?nachname=

### Prerequisites
* Having a minikube-based docker environment with postgres running (for a setup, take a look at the application.yaml file) or running in test mode where only testcontainers are used.
```
minikube start --driver docker
```
* Install the lens extensions in docker to be able to access the minikube cluster using open lens.

### Running the service locally 
```
./gradlew bootRun
```

### Creating the config map and secret (one time)
The username and secret must be identical to the running postgres container
```
cd k8s
./install.sh
cd ..
```


### Creating a container image and publish it to local docker hub
The necessary credentials for accessing docker hub on the internet can be found in 1password.
```
./gradlew bootBuildImage
```

### Deployment
```
helm install <deployment-name> deployment
```

```
helm uninstall <deployment-name>
```

### Accessing the service
The simplest way to access the service is via open lens.
* Choose the pod
* Scroll down to ports
* Click *Forward...* to oppen in browser