import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootBuildImage

plugins {
    id("org.springframework.boot") version "3.2.0"
    id("io.spring.dependency-management") version "1.1.3"
    id("io.gitlab.arturbosch.detekt") version "1.23.1"
    kotlin("jvm") version "1.9.0"
    kotlin("plugin.spring") version "1.9.0"
}

group = "de.envite.sample.spring"
version = "0.0.1-SNAPSHOT"

detekt {
    parallel = true
    config.setFrom("$rootDir/detekt-config.yml")
    buildUponDefaultConfig = true
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
    mavenCentral()
}

extra["springCloudVersion"] = "2023.0.0"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-jdbc")
    implementation("org.springframework.boot:spring-boot-testcontainers")
    implementation("org.springframework.cloud:spring-cloud-starter-kubernetes-client-all")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("net.logstash.logback:logstash-logback-encoder:7.4")
    implementation("org.flywaydb:flyway-core")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.postgresql:postgresql:42.7.0")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("io.micrometer:micrometer-tracing-bridge-otel")
    implementation("net.ttddyy.observation:datasource-micrometer-spring-boot:1.0.2")
    implementation("io.opentelemetry:opentelemetry-exporter-otlp")
    implementation("io.github.digma-ai:digma-spring-boot-micrometer-tracing-autoconf:0.7.4")
    testImplementation("com.ninja-squad:springmockk:4.0.2")
    testImplementation("org.testcontainers:junit-jupiter")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.testcontainers:postgresql:1.19.3")
    testImplementation("io.rest-assured:rest-assured")

    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.23.1")
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
    }
}

tasks {
    withType<Detekt> {
        // Target version of the generated JVM bytecode. It is used for type resolution.
        this.jvmTarget = "17"
    }

    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs += "-Xjsr305=strict"
            jvmTarget = "17"
        }
    }

    withType<Test> {
        useJUnitPlatform()
    }

    named<BootBuildImage>("bootBuildImage") {
        // onlyIf { ext["runsOnJenkins"] as Boolean }
        builder = "paketobuildpacks/builder:tiny"
        publish = true

        imageName = "enviteconsulting/${project.name}:1.0.22"

        docker {
            publishRegistry {
                url = providers.systemProperty("DOCKER_HOST")
                username = providers.systemProperty("DOCKER_REGISTRY_USERNAME")
                password = providers.systemProperty("DOCKER_REGISTRY_PASSWORD")
            }
        }
    }
}
