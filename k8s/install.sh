#!/bin/sh -ef
kubectl apply --server-side --force-conflicts -f ./config/postgres-config.yaml

kubectl apply --server-side --force-conflicts -f ./secrets/postgres-secrets.yaml