package de.envite.sample.spring.persistence

import de.envite.sample.spring.person.model.Adresse
import de.envite.sample.spring.person.model.Hausnummer
import de.envite.sample.spring.person.model.Nachname
import de.envite.sample.spring.person.model.Ort
import de.envite.sample.spring.person.model.PLZ
import de.envite.sample.spring.person.model.Person
import de.envite.sample.spring.person.model.PersonId
import de.envite.sample.spring.person.model.Strasse
import de.envite.sample.spring.person.model.Vorname
import de.envite.sample.spring.person.persistence.PersonRepository
import de.envite.sample.spring.person.persistence.PersonRepositoryConfiguration
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestConstructor
import org.springframework.test.context.TestConstructor.AutowireMode.ALL
import org.testcontainers.junit.jupiter.Testcontainers

@SpringBootTest(classes = [PersonRepositoryConfiguration::class])
@ActiveProfiles("test", "integration-test")
@Testcontainers
@TestConstructor(autowireMode = ALL)
@AutoConfigureJson
class PersonRepositoryTest(private val personRepository: PersonRepository) {
    @Test
    fun `read one person from repository`() {
        val peterParker = Person(
            PersonId.fromString("7de023ae-611a-11ee-8c99-0242ac120002"),
            Vorname("Peter"),
            Nachname("Parker"),
            Adresse(
                Strasse("Ingram Street"),
                Hausnummer("20"),
                PLZ("11375"),
                Ort("Forest Hills")
            )
        )
        personRepository.persist(peterParker)
        assertThat(personRepository.findAll().count()).isEqualTo(1)

        val listOfPeters = personRepository.findByVorname(peterParker.vorname)
        assertThat(listOfPeters.count()).isEqualTo(1)
        assertThat(listOfPeters[0].nachname.value).isEqualTo("Parker")

        val listOfParkers = personRepository.findByNachname(peterParker.nachname)
        assertThat(listOfParkers.count()).isEqualTo(1)
        assertThat(listOfParkers[0].vorname.value).isEqualTo("Peter")

        personRepository.delete(peterParker)
    }
}
