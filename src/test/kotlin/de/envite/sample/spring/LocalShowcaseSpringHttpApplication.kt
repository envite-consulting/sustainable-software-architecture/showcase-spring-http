package de.envite.sample.spring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.fromApplication

@SpringBootApplication
class LocalShowcaseSpringHttpApplication

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    fromApplication<ShowcaseSpringHttpApplication>()
        .with(LocalTestContainerConfig::class.java)
        .run(*args)
}
