package de.envite.sample.spring

import de.envite.sample.spring.person.persistence.PersonRepositoryConfiguration
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.utility.DockerImageName

@TestConfiguration
@Import(PersonRepositoryConfiguration::class)
class LocalTestContainerConfig {
    @Bean
    @ServiceConnection
    fun postgresContainer() =
        PostgreSQLContainer(
            DockerImageName
                .parse("postgres:15.2")
                .asCompatibleSubstituteFor("postgres")
        )
}
