package de.envite.sample.spring.presentation
import com.fasterxml.jackson.databind.ObjectMapper
import de.envite.sample.spring.LocalShowcaseSpringHttpApplication
import de.envite.sample.spring.person.model.Adresse
import de.envite.sample.spring.person.model.Hausnummer
import de.envite.sample.spring.person.model.Nachname
import de.envite.sample.spring.person.model.Ort
import de.envite.sample.spring.person.model.PLZ
import de.envite.sample.spring.person.model.Person
import de.envite.sample.spring.person.model.PersonId
import de.envite.sample.spring.person.model.Strasse
import de.envite.sample.spring.person.model.Vorname
import de.envite.sample.spring.person.persistence.PersonRepository
import io.restassured.RestAssured
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestConstructor
import org.springframework.test.context.TestConstructor.AutowireMode.ALL
import org.testcontainers.junit.jupiter.Testcontainers

@SpringBootTest(classes = [LocalShowcaseSpringHttpApplication::class], webEnvironment = RANDOM_PORT)
@ActiveProfiles("test", "integration-test")
@Testcontainers
@TestConstructor(autowireMode = ALL)
class PersonRestControllerTest(
    private val personRepository: PersonRepository,
    private val mapper: ObjectMapper
) {
    val peterParker = Person(
        PersonId.fromString("7de023ae-611a-11ee-8c99-0242ac120002"),
        Vorname("Peter"),
        Nachname("Parker"),
        Adresse(
            Strasse("Ingram Street"),
            Hausnummer("20"),
            PLZ("11375"),
            Ort("Forest Hills")
        )
    )

    @LocalServerPort
    var port = 0

    @BeforeEach
    internal fun setUp() {
        personRepository.persist(peterParker)
        RestAssured.port = port
    }

    @Test
    internal fun `can read all person`() {
        val response = given().get("/persons")
            .then()
            .contentType(ContentType.JSON)
            .statusCode(HttpStatus.OK.value())
            .extract()
            .asString()
        assertThat(response).isEqualTo(mapper.writeValueAsString(listOf(peterParker)))
    }

    @AfterEach
    internal fun tearDown() {
        personRepository.delete(peterParker)
    }
}
