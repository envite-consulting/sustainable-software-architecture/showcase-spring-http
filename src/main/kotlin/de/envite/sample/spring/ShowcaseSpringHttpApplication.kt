package de.envite.sample.spring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShowcaseSpringHttpApplication

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<ShowcaseSpringHttpApplication>(args = args)
}
