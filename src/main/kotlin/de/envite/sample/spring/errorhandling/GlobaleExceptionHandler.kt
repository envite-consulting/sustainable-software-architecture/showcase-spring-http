package de.envite.sample.spring.errorhandling

import de.envite.sample.spring.logging.log
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class GlobaleExceptionHandler {

    @ExceptionHandler(ApplicationException::class)
    internal fun applicationException(exception: ApplicationException): ResponseEntity<ErrorResponse> {
        return ResponseEntity
            .status(exception.errorCode.statusCode)
            .body(exception.toResponse())
    }

    @ExceptionHandler(Throwable::class)
    internal fun serverError(exception: Throwable): ResponseEntity<ErrorResponse> {
        // TODO add context to the error log
        log.error("An unexpected exception occurred.", exception)
        return ResponseEntity.internalServerError()
            .body(ErrorResponse(ErrorCode.INTERNAL_ERROR))
    }
}

private fun ApplicationException.toResponse() = ErrorResponse(errorCode, message)
