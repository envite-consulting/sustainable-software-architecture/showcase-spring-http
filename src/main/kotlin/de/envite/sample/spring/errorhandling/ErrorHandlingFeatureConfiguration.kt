package de.envite.sample.spring.errorhandling

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(GlobaleExceptionHandler::class)
class ErrorHandlingFeatureConfiguration
