package de.envite.sample.spring.errorhandling

import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.BAD_GATEWAY
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR

enum class ErrorCode(val statusCode: HttpStatus, val id: String, val msg: String) {
    DATABASE_ERROR(BAD_GATEWAY, "6001", "Error accessing the database"),
    PERSON_NOT_FOUND(HttpStatus.BAD_REQUEST, "6002", "Person could not be found"),
    INTERNAL_ERROR(INTERNAL_SERVER_ERROR, "6666", "Something unexpected happens."),
}
