package de.envite.sample.spring.errorhandling

internal data class ErrorResponse(
    val errorCode: ErrorCode,
    val msg: String = errorCode.msg,
    val id: String = errorCode.id
)
