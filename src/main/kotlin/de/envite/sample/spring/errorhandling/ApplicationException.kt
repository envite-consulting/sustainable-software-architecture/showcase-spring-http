package de.envite.sample.spring.errorhandling

open class ApplicationException(
    val errorCode: ErrorCode,
    override val message: String,
    override val cause: Throwable? = null
) : Exception()
