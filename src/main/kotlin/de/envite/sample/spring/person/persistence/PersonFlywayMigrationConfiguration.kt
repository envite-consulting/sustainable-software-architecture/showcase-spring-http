package de.envite.sample.spring.person.persistence

import org.flywaydb.core.Flyway
import org.springframework.beans.factory.InitializingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import javax.sql.DataSource

@Configuration
@Profile(value = ["cloud", "local", "test"])
class PersonFlywayMigrationConfiguration {
    @Bean
    fun flywayMigrationInitializer(personDbDataSource: DataSource): InitializingBean = InitializingBean {
        Flyway.configure()
            .dataSource(personDbDataSource)
            .locations("classpath:db/migration")
            .table("flyway_schema_history")
            .load()
            .migrate()
    }
}
