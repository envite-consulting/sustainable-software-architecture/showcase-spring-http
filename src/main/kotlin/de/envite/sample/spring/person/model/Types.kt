package de.envite.sample.spring.person.model

import de.envite.sample.spring.common.NonBlankString
import de.envite.sample.spring.common.RegexCheckedString
import de.envite.sample.spring.common.TypedUUID
import java.util.UUID

class PersonId(value: UUID) : TypedUUID(value) {
    companion object {
        fun random() = UUID.randomUUID().let(::PersonId)
        fun fromString(v: String) = UUID.fromString(v).let(::PersonId)
    }
}

class Vorname(value: String) : NonBlankString(value)
class Nachname(value: String) : NonBlankString(value)

class Person(val id: PersonId, val vorname: Vorname, val nachname: Nachname, val adresse: Adresse)

class Strasse(value: String) : NonBlankString(value)
class Hausnummer(value: String) : NonBlankString(value)
class PLZ(value: String) : RegexCheckedString(value, Regex("\\d{5}"))
class Ort(value: String) : NonBlankString(value)

data class Adresse(val strasse: Strasse, val hausnummer: Hausnummer, val plz: PLZ, val ort: Ort)
