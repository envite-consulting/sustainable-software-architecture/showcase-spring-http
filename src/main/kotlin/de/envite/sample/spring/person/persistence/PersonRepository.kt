package de.envite.sample.spring.person.persistence

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.envite.sample.spring.logging.log
import de.envite.sample.spring.logging.tv
import de.envite.sample.spring.person.model.Adresse
import de.envite.sample.spring.person.model.Nachname
import de.envite.sample.spring.person.model.Person
import de.envite.sample.spring.person.model.PersonId
import de.envite.sample.spring.person.model.Vorname
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import org.springframework.stereotype.Repository
import java.sql.ResultSet

private const val PERSON_TABLENAME = "person"

@Repository
@Suppress("TooManyFunctions")
class PersonRepository(
    private val jdbcOperations: NamedParameterJdbcOperations,
    private val mapper: ObjectMapper
) {
    private val personRowMapper = PersonRowMapper(mapper)

    fun findAll(): List<Person> {
        return handleDbErrors {
            jdbcOperations.query(
                "select * from $PERSON_TABLENAME",
                personRowMapper
            ).filterNotNull()
        }
    }

    fun findById(id: PersonId): Person? = handleDbErrors {
        val query = """
                select * from $PERSON_TABLENAME
                id = :id
        """
        jdbcOperations.query(query, mapOf("id" to id.value), personRowMapper).singleOrNull()
    }

    fun findByVorname(vorname: Vorname): List<Person> {
        return handleDbErrors {
            jdbcOperations.query(
                "select * from $PERSON_TABLENAME where vorname = :vorname",
                mapOf("vorname" to vorname.value),
                personRowMapper
            ).filterNotNull()
        }
    }

    fun findByNachname(nachname: Nachname): List<Person> {
        return handleDbErrors {
            jdbcOperations.query(
                "select * from $PERSON_TABLENAME where nachname = :nachname",
                mapOf("nachname" to nachname.value),
                personRowMapper
            ).filterNotNull()
        }
    }

    fun persist(person: Person) = handleDbErrors {
        val insertQuery = """
            INSERT INTO $PERSON_TABLENAME 
            VALUES (:id, :vorname, :nachname, :adresse::json)
        """.trimIndent()
        jdbcOperations.update(insertQuery, person.asMap())
        log.debug("Created new inbox entry [{}].", tv("personId", person.id))
    }

    fun delete(person: Person) = handleDbErrors {
        val deleteQuery = """
            DELETE FROM $PERSON_TABLENAME WHERE id = :id
        """.trimIndent()
        jdbcOperations.update(deleteQuery, mapOf("id" to person.id.value))
        log.debug("Created new inbox entry [{}].", tv("personId", person.id))
    }

    private fun Person.asMap(): Map<String, Any?> = mutableMapOf(
        "id" to id.value,
        "vorname" to vorname.value,
        "nachname" to nachname.value,
        "adresse" to mapper.writeValueAsString(adresse)
    )
}

internal class PersonRowMapper(private val mapper: ObjectMapper) : RowMapper<Person> {
    override fun mapRow(rs: ResultSet, rowNum: Int): Person {
        return Person(
            id = rs.getTypedUUID("id"),
            vorname = rs.getTypedString("vorname"),
            nachname = rs.getTypedString("nachname"),
            adresse = rs.getAdresse()
        )
    }

    private fun ResultSet.getAdresse(): Adresse {
        val json = getString("adresse")
        return mapper.readValue<Adresse>(json)
    }
}
