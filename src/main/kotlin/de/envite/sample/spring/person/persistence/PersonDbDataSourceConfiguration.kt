package de.envite.sample.spring.person.persistence

import com.zaxxer.hikari.HikariDataSource
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.transaction.TransactionManager
import javax.sql.DataSource

@Configuration
@EnableConfigurationProperties(DataSourceProperties::class)
class PersonDbDataSourceConfiguration {

    @Bean
    @ConfigurationProperties("datasources.person.datasource")
    fun personDbDataSourceProperties() = DataSourceProperties()

    @Bean
    @Profile("!local")
    @ConfigurationProperties("datasources.person.datasource.hikari")
    fun personDbDataSource(
        personDbDataSourceProperties: DataSourceProperties
    ): HikariDataSource {
        val dataSource = personDbDataSourceProperties.initializeDataSourceBuilder()
            .type(HikariDataSource::class.java)
            .build()
        dataSource.poolName = "person-db"
        dataSource.validate()
        return dataSource
    }

    @Bean
    fun personNamedParameterJdbcOperations(personDbDataSource: DataSource): NamedParameterJdbcOperations =
        NamedParameterJdbcTemplate(personDbDataSource)

    @Bean
    fun inboxTransactionManager(personDbDataSource: DataSource): TransactionManager =
        DataSourceTransactionManager(personDbDataSource)
}
