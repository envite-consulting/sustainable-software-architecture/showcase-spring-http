package de.envite.sample.spring.person.persistence

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Import(
    PersonRepository::class,
    PersonDbDataSourceConfiguration::class,
    PersonFlywayMigrationConfiguration::class
)
@Configuration
class PersonRepositoryConfiguration
