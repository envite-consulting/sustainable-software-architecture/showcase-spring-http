package de.envite.sample.spring.person.presentation

import de.envite.sample.spring.person.model.Nachname
import de.envite.sample.spring.person.model.Vorname
import de.envite.sample.spring.person.persistence.PersonRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class PersonRestController(val repository: PersonRepository) {

    @GetMapping("/persons")
    fun getAllPersons() = repository.findAll()

    @RequestMapping("/persons", params = ["vorname"], method = [GET])
    fun getPersonsByVorname(@RequestParam vorname: Vorname) = repository.findByVorname(vorname)

    @RequestMapping("/persons", params = ["nachname"], method = [GET])
    fun getPersonsByNachname(@RequestParam nachname: Nachname) = repository.findByNachname(nachname)
}
