@file:Suppress("TooManyFunctions")

package de.envite.sample.spring.person.persistence

import de.envite.sample.spring.common.TypedString
import de.envite.sample.spring.common.TypedUUID
import de.envite.sample.spring.errorhandling.ApplicationException
import de.envite.sample.spring.errorhandling.ErrorCode.DATABASE_ERROR
import org.springframework.dao.DataAccessException
import org.springframework.dao.TransientDataAccessException
import java.sql.ResultSet
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import kotlin.reflect.full.primaryConstructor

internal inline fun <reified T> ResultSet.getList(columnName: String): List<T>? {
    return (getArray(columnName)?.array as? Array<*>)
        ?.filterNotNull()
        ?.map { it as T }
        ?.toList()
}

@Throws(DataAccessException::class, ApplicationException::class)
internal fun <T> handleDbErrors(block: () -> T): T = try {
    block()
} catch (ex: DataAccessException) {
    when (ex) {
        is TransientDataAccessException -> throw ApplicationException(DATABASE_ERROR, "Database not accessible.", ex)
        else -> throw ex
    }
}

internal fun ResultSet.getLocalDate(columnLabel: String): LocalDate =
    checkNotNull(
        getTimestamp(columnLabel).toLocalDateTime().toLocalDate()
    ) { "Value in [$columnLabel] was unexpectedly null." }

internal fun ResultSet.getLocalDateTime(columnLabel: String): LocalDateTime =
    checkNotNull(
        getTimestamp(columnLabel).toLocalDateTime()
    ) { "Value in [$columnLabel] was unexpectedly null." }

internal inline fun <reified T : TypedString> ResultSet.getNullableTypedString(columnLabel: String): T? =
    getString(columnLabel)?.let { T::class.primaryConstructor?.call(it) }

internal inline fun <reified T : TypedString> ResultSet.getTypedString(columnLabel: String): T =
    checkNotNull(
        getNullableTypedString(columnLabel)
    ) { "Value in [$columnLabel] was unexpectedly null." }

internal inline fun <reified T : TypedUUID> ResultSet.getNullableTypedUUID(columnLabel: String): T? =
    getString(columnLabel)?.let { T::class.primaryConstructor?.call(UUID.fromString(it)) }

internal inline fun <reified T : TypedUUID> ResultSet.getTypedUUID(columnLabel: String): T =
    checkNotNull(
        getNullableTypedUUID(columnLabel)
    ) { "Value in [$columnLabel] was unexpectedly null." }
