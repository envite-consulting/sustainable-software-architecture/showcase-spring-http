package de.envite.sample.spring.logging

import de.envite.sample.spring.common.TypedValue
import net.logstash.logback.argument.StructuredArguments
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

private val loggers = mutableMapOf<KClass<out Any>, Logger>()

val Any.log: Logger
    get() {
        return loggers.getOrPut(this::class) {
            LoggerFactory.getLogger(this.javaClass)
        }
    }

fun tv(key: String, value: TypedValue<*>) = StructuredArguments.v(key, value.value)!!
fun tkv(key: String, value: TypedValue<*>) = StructuredArguments.kv(key, value.value)!!
