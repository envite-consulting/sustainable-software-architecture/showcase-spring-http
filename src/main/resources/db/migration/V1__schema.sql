create table person
(
    id           uuid        primary key not null,
    vorname      varchar(64) not null,
    nachname     varchar(64) not null,
    adresse      jsonb not null
);